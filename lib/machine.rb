#!/usr/bin/env ruby

require 'product'
require 'purchase'
require 'pry'

class Machine
  attr_reader :state
  attr_reader :inner_change, :products

  def initialize
    @state = :stopped
    # @products = { <pos> => { product: <Product>, amount: <num> } }
    @products = {}
    @inner_change = {
      '1p' => { display: '1p', value: 1, coins_amount: 0 },
      '2p' => { display: '2p', value: 2, coins_amount: 0 },
      '5p' => { display: '5p', value: 5, coins_amount: 0 },
      '10p' => { display: '10p', value: 10, coins_amount: 0 },
      '20p' => { display: '20p', value: 20, coins_amount: 0 },
      '50p' => { display: '50p', value: 50, coins_amount: 0 },
      'p1' => { display: 'p1', value: 100, coins_amount: 0 },
      'p2' => { display: 'p2', value: 200, coins_amount: 0 }
    }
  end


  ## change => Hash { '1p' => <coins amount>, '2p' => <coins_amount> }
  def load_change(loaded_change)
    withdraw_change
    loaded_change.each do |denomination, amount|
      @inner_change[denomination][:coins_amount] = amount
    end
  end

  ## products => Array of hashes [{ position: 1, name: '', amount: 0, price_cents: 0 }]
  def load_products(loaded_products)
    @products = {}
    loaded_products.each_with_index do |product, index|
      built_product = Product.new(product[:name], product[:price_cents])

      @products[product[:position]] = { product: built_product, amount: product[:amount] }
    end
  end

  ## coins_inserted - Hash { <coin_denomination> => amount } e.g. { '1p' => 10 }
  def sell(item_position, coins_inserted)
    money_inserted = 0
    coins_inserted.each { |coin, amount| money_inserted += inner_change[coin][:value] * amount  }

    return no_product_available(item_position) unless validate_amount(item_position)
    return not_enough_money(item_position, money_inserted) unless validate_money(item_position, money_inserted)
    return not_enough_change(item_position, money_inserted) unless validate_enough_change(item_position, money_inserted)

    product = @products[item_position][:product]
    change_info  = calculate_change(product, money_inserted, coins_inserted)

    if change_info[:leftover] != 0
      top_up_change!(change_info[:coins])
      top_down_change!(coins_inserted)
      return not_enough_change(item_position, money_inserted)
    end
    decrease_product_amount!(item_position)
    Purchase.new(product: product.name, change: change_info[:coins], status: :success)
  end

  private

  def withdraw_change
    inner_change.each do |key, change|
      change[:coins_amount] = 0
    end
  end

  def top_up_change!(coins)
    coins.each do |coin, amount|
      inner_change[coin][:coins_amount] += amount
    end
  end

  def top_down_change!(coins)
    coins.each do |coin, amount|
      inner_change[coin][:coins_amount] -= amount
    end
  end

  def validate_enough_change(item_position, money_inserted)
    available_change = inner_change.values.reduce(0) { |sum, item| sum += item[:coins_amount] * item[:value] }
    available_change + products[item_position][:product].price >= money_inserted
  end

  def not_enough_change(item_position, money_inserted)
    Purchase.new(
      product: products[item_position][:product].name,
      change: money_inserted,
      status: :failed,
      error: 'Not enough change in the machine'
    )
  end

  def decrease_product_amount!(item_position)
    products[item_position][:amount] -= 1
  end

  def calculate_change(product, money_inserted, coins_inserted)
    top_up_change!(coins_inserted)

    change = money_inserted - product.price
    leftover_change = change

    change_distribution = {}

    inner_change.values.sort_by {|item| -item[:value] }.each do |coin_info|
      while leftover_change > 0
        break if coin_info[:coins_amount] < 1
        break if coin_info[:value] > leftover_change

        leftover_change -= coin_info[:value]
        change_distribution[coin_info[:display]] ||= 0
        change_distribution[coin_info[:display]] += 1

        inner_change[coin_info[:display]][:coins_amount] -= 1
      end
    end

    {coins: change_distribution, leftover: leftover_change }
  end


  def validate_amount(item_position)
    products[item_position][:amount] > 0
  end

  def no_product_available(item_position)
    Purchase.new(
      product: products[item_position][:product].name,
      change: 0,
      status: :failed,
      error: 'Product is not available'
    )
  end

  def validate_money(item_position, money_inserted)
    products[item_position][:product].price <= money_inserted
  end

  def not_enough_money(item_position, money_inserted)
    Purchase.new(
      product: products[item_position][:product].name,
      change: money_inserted,
      status: :failed,
      error: 'Not enough money inserted'
    )
  end
end
