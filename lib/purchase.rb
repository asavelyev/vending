class Purchase
  attr_reader :product, :change, :status, :error
  def initialize(product:, change:, status:, error: nil)
    @product = product
    @change = change
    @status = status
    @error = error
  end
end
