require 'spec_helper'

describe Machine do
  subject(:machine) { described_class.new }

  describe '#sell' do
    let(:amount) { 1 }
    let(:products) do
      [
        { position: 1, name: 'mars', amount: amount, price_cents: 100},
      ]
    end

    before do
      machine.load_products(products)
    end

    context 'when purchase successful' do
      let(:change) { { '1p' => 3, '2p' => 2, '5p' => 1, '10p' => 0, '20p' => 1 } }
      let(:coins_inserted) { { 'p1' => 1, '10p' => 1 } }

      before { machine.load_change(change) }

      it 'returns product and chage' do
        result = machine.sell(1, coins_inserted)

        expect(result.status).to eq(:success)
        expect(result.error).to be_nil
        expect(result.change).to include({'10p' => 1})
        expect(result.product).to eq(products.first[:name])
      end
    end

    context 'when not enough products' do
      let(:amount) { 0 }

      it 'returns error' do
        result = machine.sell(1, { 'p2' => 1 })

        expect(result.status).to eq(:failed)
        expect(result.error).to eq('Product is not available')
        expect(result.change).to eq(0)
        expect(result.product).to eq(products.first[:name])
      end
    end

    context 'when not enough money inserted' do
      it 'returns error' do
        result = machine.sell(1, {'50p' => 1, '20p' => 2})

        expect(result.status).to eq(:failed)
        expect(result.error).to eq('Not enough money inserted')
        expect(result.change).to eq(90)
        expect(result.product).to eq(products.first[:name])
      end
    end

    context 'when not enough change ' do
      context 'when actual change is less than change to give back' do
        let(:change) { { '1p' => 1, '2p' => 2 } }

        before { machine.load_change(change) }

        it 'returns error' do
          result = machine.sell(1,  { 'p2' => 1 })

          expect(result.status).to eq(:failed)
          expect(result.error).to eq('Not enough change in the machine')
          expect(result.change).to eq(200)
          expect(result.product).to eq(products.first[:name])
        end
      end

      context 'when not enough right coins to give back' do
        let(:change) { {'1p' => 3, '2p' => 2, '5p' => 1, '10p' => 3, 'p1' => 1 } }
        let(:products) do
          [
            { position: 1, name: 'mars', amount: amount, price_cents: 150},
          ]
        end

        before { machine.load_change(change) }

        it 'returns error' do
          result = machine.sell(1,  { 'p2' => 1 })

          expect(result.status).to eq(:failed)
          expect(result.error).to eq('Not enough change in the machine')
          expect(result.change).to eq(200)
          expect(result.product).to eq(products.first[:name])
        end
      end
    end
  end

  describe '#load_change' do
    let(:change) { { '1p' => 100, '2p' => 50 } }

    before { machine.load_change(change) }

    it 'updates change information' do
      expect(machine.inner_change['1p'][:coins_amount]).to eq(100)
      expect(machine.inner_change['2p'][:coins_amount]).to eq(50)
      expect(machine.inner_change['5p'][:coins_amount]).to eq(0)
    end
  end

  describe '#load_products' do
    let(:products) do
      [
        { position: 1, name: 'mars', amount: 10, price_cents: 100},
      ]
    end

    before do
      machine.load_products(products)
    end

    it 'updates products records' do
      expect(machine.products[1][:amount]).to eq(10)
      expect(machine.products[1][:product].name).to eq('mars')
      expect(machine.products[1][:product].price).to eq(100)
      expect(machine.products[2]).to be_nil
    end
  end
end
