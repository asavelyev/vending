# Vending machine

## How to use

run `irb -Ilib` from the `vending` directory

```
require 'machine'
require 'product'
require 'purchase'
m = Machine.new
m.load_change({'1p' => 3, '2p' => 2, '5p' => 1, '10p' => 3, 'p1' => 1 })


products =  [
        { position: 1, name: 'mars', amount: 2, price_cents: 90},
        { position: 2, name: 'snickers', amount: 1, price_cents: 80},
        { position: 3, name: 'twix', amount: 1, price_cents: 150}
      ]

m.load_products(products)

m.sell(3, {'p2' => 2})
m.sell(1, {'p1' => 1})
m.sell(2, {'p1' => 1})
```

## Notes

- There are validation on input data.
- I haven't implemented any exceptions as alternative to Purchase class with status
- I haven't had enough time to thing how to break down the main `Machine` class into few smaller classes
